
let checkBoxForAllCustomApp=`div[ng-click="select(item, $event, $index);"]>label>input`
let deleteIcon =`.btn-group > [type="button"] > .icon > use`
let okInDeleteDialog =`#confirm > .modal-dialog > .modal-content > .popup-buttons > .btn-save`
let addIcon =`[ng-show="tableConfig.addAction"] > .icon`
let nameField =`input[ng-model="newApp.name"]`
let describtionField =`div[class="modal-content"] > div[class="modal-body"] >div >form-input >div > div > div>textarea`
let okInAddDialog =`#addAppBtn`
let customApp ='.item'
let projectsName =`div[class="info"] > div[ng-class="{'name-width' : item.status != null}"] >span`
let projecrtsDescribtion =`div[class="info"] > div[class="description ellipsis"] >span`
let general =`a[aria-controls="general"]`
let nameInGeneral =`div > div > input[type ="text"][ng-model="app.name"]`
let describtionInGeneral =`textarea[ng-model="app.description"]`
let save = 'save-button > .btn'
let addOrEditCategory = `div[ng-click="addNew($event)"] > span`
//JSDoc
/**
 * find the selector for the specifed check box category
 * @author Omar Salameh
 * @param {string} category - the name of category 
 * @returns {string} -  the selector for check box for the specifed category
 * @example cy.get(Category('Abortion')).click()
 * @see https://test.catonet.works/#!/1656/customApplications for the category that available
 */
let Categoey =function(category){return `input[id="chck${category}"] `}

let okInCategoryDialog = `#categoriesServicesDlg > .modal-dialog > .modal-content > .popup-buttons > .btn-save`
let addedCategory =`div[class="entry-list-container"]>div>div>span>span`
let crossIconForCategory =`svg[class="icon icon-tag-x"]`
let allCheckBoxInCategoryDialog =`div[class="checkbox no-disable"] > label >span> span[class="check"]`
let checkBoxForAbortion =`input[id="chckAbortion"]`
let cancelButtonInAddCategoryDialog =`button[ng-click="$root.closePopup('categoriesServicesDlg')"]`
let crossIconInAddCategoryDialog =`div[ng-click="$root.closePopup($ctrl.rulesType + 'networkingRulesServicesDlg')"]`
let crossIconForResolutionDialog = `div[ng-click="$root.confirmCancel()"]`
export {crossIconForResolutionDialog,crossIconInAddCategoryDialog,cancelButtonInAddCategoryDialog,checkBoxForAbortion,allCheckBoxInCategoryDialog,crossIconForCategory,addedCategory,okInCategoryDialog,Categoey,addOrEditCategory,save,describtionInGeneral,nameInGeneral,general,projecrtsDescribtion,projectsName,customApp,checkBoxForAllCustomApp,deleteIcon,okInDeleteDialog,addIcon,nameField,describtionField,okInAddDialog}