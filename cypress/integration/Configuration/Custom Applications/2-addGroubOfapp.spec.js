import  {logInToCatoUsingRequest, LogInToCatoManually} from '../../../support/login.js'
import {crossIconForResolutionDialog,projecrtsDescribtion,projectsName,customApp,checkBoxForAllCustomApp,deleteIcon,okInDeleteDialog,addIcon,nameField,describtionField,okInAddDialog} from '../../../support/customApp.js'

describe('add Groub of custom app',function(){

  before(function(){
 logInToCatoUsingRequest()
 cy.MaximizeWindwoSize()
 cy.get(crossIconForResolutionDialog).click({force:true})//in case the resolution dialog pop up
})

    it('add 100 custom app',()=>{
        for(let i=0;i<100;i++){
            cy.get(addIcon).click()
            cy.get(nameField).type('p'+i)
            cy.get(describtionField).type('description')
            
            cy.get(okInAddDialog).click({force:true})
    
            cy.server()
            cy.route('POST','getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
            // wait for GET getCustomAppDefinitions?customapplicationId=.*
            cy.wait('@addedSuccessfully').its('status').should('eq', 200)   
        }
    })

    
    it.only('delete all custom app',function(){
        cy.get(checkBoxForAllCustomApp).each(function(elem){

            elem.click()
        })
        cy.get(deleteIcon).click()
        cy.get(okInDeleteDialog).click()

        cy.server()
        cy.route('POST', 'getAllCustomApps?accountId=*').as('addedSuccessfully')
        // wait for GET getAllCustomApps?accountId==.*
        cy.wait('@addedSuccessfully').its('status').should('eq', 200)
        
    })

})