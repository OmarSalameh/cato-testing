//this test assume nothing
//at the end there will be one app added with no member


// type definitions for Cypress object "cy"
/// <reference types="cypress" />

// type definitions for custom commands like "logInToCatoUsingRequist"
/// <reference types="../../../support" />


import  {logInToCatoUsingRequest, LogInToCatoManually} from '../../../support/login.js'
import {crossIconForResolutionDialog,projecrtsDescribtion,projectsName,customApp,checkBoxForAllCustomApp,deleteIcon,okInDeleteDialog,addIcon,nameField,describtionField,okInAddDialog} from '../../../support/customApp.js'

describe('add and verifiy', function () {

    beforeEach(() => {
        logInToCatoUsingRequest()
        cy.MaximizeWindwoSize()
        cy.get(crossIconForResolutionDialog).click({force:true})//in case the resolution dialog pop up
 
    })

    it('add a new custom application', () => {
        cy.fixture('addCustom.json').then((NewApp)=>{

       

        cy.wait(500)
        //delete all custom app:
        cy.get(checkBoxForAllCustomApp).each(function(elem){

            elem.click()
        })
        cy.get(deleteIcon).click()
        cy.get(okInDeleteDialog).click()

        cy.wait(500)
        //add custom app:

        cy.get(addIcon).click()
        cy.get(nameField).type(NewApp.name)
        cy.get(describtionField).type(NewApp.description)
            
        cy.get(okInAddDialog).click()

        cy.server()
        cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
        // wait for GET getCustomAppDefinitions?customapplicationId=.*
        cy.wait('@addedSuccessfully').its('status').should('eq', 200)


        cy.get(customApp).should('have.length', 1)//assert that one and only one custom app added


          //check all projects name and assert if there is a project with name 'project1'
          cy.get(projectsName).should('contain.text', NewApp.name)

          //check all projects description and assert if there is a project with description 'project description'
          cy.get(projecrtsDescribtion).should('contain.text', NewApp.description)
  

    })
})

})
